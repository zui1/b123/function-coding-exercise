let array = ["2,500", "4,561", "1,302", "7,123"]

function removeComma() {

    let newArray = []

    array.forEach(i => {

        //console.log(i)  
        let noComma = ""

        for (let a = 0; a < i.length; a++) {

            if (i[a] !== ",") {

                noComma += i[a]
                array = parseInt(noComma)

            }
        }
        newArray.push(array)
    })
    return newArray
}

console.log(removeComma())

let names = ["Sally", "Jane", "Marcus", "Arthur", "Marcus", "Connie", "Sally"]

function noMultipleEntries(name) {

    var newArray = [];

    for (var i = 0; i < name.length; i++) {

        if (newArray.indexOf(name[i]) === -1) {

            newArray.push(name[i]);

        }
    }
    return newArray
}

console.log(noMultipleEntries(names))

function convertMinutestoSeconds(minutes) {

    return Math.floor(minutes * 60);

}

console.log(convertMinutestoSeconds(2))